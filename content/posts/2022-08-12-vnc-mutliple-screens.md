---
author: Haquor
date: 2022-08-12T17:29:01-04:00
draft: false
title: "Virtual Desktops for Local Users on OSX"
type: post
url: /2022/08/12/virtual-desktop-accounts/
image: "enable-ssh.png"
---

In the world of threat intelligence, sometimes you need to pull a lot of data, quickly. Downloading data manually is tedious, so most hackers and programmers turn to web scraping to solve their problems.

Many modern websites use [anti-bot detection mechanisms](https://datadome.co/bot-management-protection/bot-detection-how-to-identify-bot-traffic-to-your-website/#identify) to block automated scraping tools, and to make matters worse they can actually detect whether you're really opening your web browser's GUI, so running Chrome [headless](https://developer.chrome.com/blog/headless-chrome/) isn't an option.

Nope, you've got to open every single page you'd like to pull data from, browse around, pull your target info and close it.

Some operating systems allow you to simulate GUI interaction without actually having the windows open in your main UI session, [see X11 usage](https://blog.testproject.io/2018/02/20/chrome-headless-selenium-python-linux-servers/). But since not Mac OSX didn't run Xorg, I had to create a workaround.
<!--more-->

### Using a secondary account

My first idea was to create a second user account to run the web scraper session from. This works, but it's inconvenient. If you want to check the progress or see if your scraper has run into any snags, you've got to fully switch users.

Now, OSX has recently simplified this process with [faster user switching](https://support.apple.com/guide/mac-help/switch-quickly-between-users-mchlp2439/mac), which, with a fingerprint is a pretty painless experience.

But I really couldn't let go of the idea of accessing other accounts with a quick swipe. Something I like to do is set up completely separate user accounts for different projects. For example, having a dedicated user account that asks as my pentesting lab has been quite helpful.

I wanted a quicker, cleaner solution that allowed me to continue my workflow without effectively "logging out" of my main account.

### VNC remote viewing via SSH

I decided to create a solution that allow me to enter screen sharing sessions with other local user accounts.

This required a service to host the screen share, and VNC was stable, cross-platform and easy. I also found that OSX has built-in screen sharing capability built on VNC.

To enable it, I simply opened **System Preferences -> Sharing** and enabled Screen Sharing for the relevant users.

{{< fluid_imgs
  "pure-u-1-1|/screenshare.png|Screenshare Menu"
>}}

However, how would I access a VNC share on the same computer? VNC listens on port 5900, and if I open OSX screen sharing (which can be found in /System/Library/CoreServices/Applications), it gives me this:

{{< fluid_imgs
  "pure-u-1-1|/screenshare-error.png|Screenshare Error"
>}}

Since I couldn't connect to myself directly, the solution was using [SSH local forwarding](https://linuxize.com/post/how-to-setup-ssh-tunneling/) to forward all traffic destined for port 5901 to port 5900 (VNC).

    ssh -NL 5901:localhost:5900 localhost &

Then, I could connect to port 5901 on my local machine instead, and all the traffic would actually go to the listening VNC service and allow me to view my own screen.

The other important part here, was to make sure SSH was enabled by navigating to **System Preferences -> Remote Login** and enabling SSH for the relevant users.

{{< fluid_imgs
  "pure-u-1-1|/enable-ssh.png|Enable SSH"
>}}

Once this was done, I could login with my secondary account selecting 'Login as myself' to access the desktop.

Since all the user traffic was local, there was no real latency making the viewing experience quite smooth.

To integrate it with OSX's virtual desktop feature, I could Full Screen the window and drag it to its own virtual space allowing me to swipe back and forth to periodically check the results of my scraper.
