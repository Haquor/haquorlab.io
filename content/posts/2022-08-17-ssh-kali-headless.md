---
author: Haquor
title: "How to SSH into a Kali VirtualBox"
date: 2022-08-17T13:41:51-04:00
draft: false
type: post
url: /2022/08/12/ssh-kali-headless
image: "ssh-kali.png"
---

Modern virtualization packages incredible power with unparalleled value.

Integrating these systems presents new challenges for my workflow. Most recently, I've found myself doing a lot of work in Kali Linux through Terminal. So I decided to make a few quick tweaks to run Kali Linux headless and access it via SSH.

I chose SSH because it can phase out passwords, replacing them with a pair of SSH keys, based around the idea of [PGP](https://www.varonis.com/blog/pgp-encryption). One key is public and one is private. The keys authenticate both parties, safely creating an encrypted tunnel for data sharing.

So we generate a public/private key pair, and we keep the private key to ourselves. We keep it secret, but we **broadcast** our public key, also known as our signature.

We ensure that our SSH server only authorizes the private key owner that matches our public key. This is how we verify our signature.

When SSH uses public-key-only authentication our passwords are never sent across the network. Instead, our private keys guard themselves with a passphrase. Knowing this passphrase allows us to sign messages proving our identity.

Having no need for password based authentication, PGP reduces the risk of a 'Man in The Middle' attack. Instead, our own signatures seal the deal with authenticity and confidentiality.

### Generate keys (on the host)

Hop into the terminal on your **host** machine and run:
```bash
    ssh-keygen
```
Follow the on-screen [instructions](https://www.ssh.com/academy/ssh/keygen) and give the key-pair a name related to your VM. It should be stored in ~/.ssh/. This directory is important, so don't go deleting files randomly or you'll lose access.

Then, edit ~/.ssh/config to ensure your key identity is automatically added to your local SSH agent.

```bash
    nano ~/.ssh/config
```

And add an entry for a new host. It can be a wildcard such as the one below, or you can define your VM as the host. Make sure *IdentityFile* links to the key you just created.

    Host *
      AddKeysToAgent yes
      UseKeychain yes
      IdentityFile ~/.ssh/VM_PUBLIC_KEY

### Changing Kali's default pws

Out of the box, kali's VM image is [pretty insecure](https://www.kali.org/docs/introduction/default-credentials/).
You've got default credentials for both the **kali** user accounts and the **root** user.

The very first thing we want to do is change those passwords by running the following prompts on our VM
```bash
    sudo passwd kali
    sudo passwd root
```
Set a strong password and continue forward.
<!--more-->

### Configuring SSH (on the VM)

Since I'm using kali, the **ssh**  service comes installed by default. All I have to do is configure it and turn it on.

Host keys are the keys a server needs to identify itself to visitors. This key should be unique, and kali [auto-generates host keys](https://www.kali.org/docs/general-use/ssh-configuration/) if they're ever missing. Be aware of this, and if you don't want them to autogenerate, you can create your own.

The SSH configuration files are at **/etc/ssh**
- ssh_config - sshd_config file for the SSH client
- sshd_config - config file for the SSH server

So go ahead and edit **sshd_config**
```bash
    sudo nano /etc/ssh/sshd_config
```
There are a few lines you should tweak. Uncomment the following by removing the # from the beginning.

    PubkeyAuthentication yes
    AuthorizedKeysFile	.ssh/authorized_keys .ssh/authorized_keys2
    PasswordAuthentication yes

We'll change that last property after we send over the SSH keys.

### Enable SSH (on the VM)

Continuing, we want to start the SSH server, but we also want it enabled so it starts on boot.
```bash
    sudo systemctl enable ssh
    sudo systemctl start ssh
```

At this point, the SSH service is configured and your passwords aren't default. You want to expose it to your host, so you can connect to SSH, but you want to do it as securely as possible.

### Configuring the VirtualBox Network

There are two main choices for the [Network Adapter settings](https://www.nakivo.com/blog/virtualbox-network-setting-guide/) on your kali VM, you've got **NAT mode** and **Bridged mode**.

- **NAT** - Your VM sits in a separate subnet. It may end up being the same as the host subnet, but not a guarantee.

You can access the outside network, such as the internet, but no one can reach into your VM, it's protected

- **Bridged** - Your VM will be in the same network as your host, if your host IP is 172.16.120.45 then your VM will be like 172.16.120.50.

It can be accessed by all computers in your host network.

### Choose NAT over Bridged

Some people debate this, but they shouldn't. In this case, use NAT over bridged. The reason why is that VirtualBox, especially on Mac OSX [doesn't actually have full compatibility](https://www.virtualbox.org/manual/ch06.html#network_bridged) if your host machine is attempting to bridge from a wireless adapter. Let me repeat.

If your host is using a wireless adapter (WiFi), you may deal with some serious bugs trying to set up Bridged mode correctly.

Instead, use NAT mode, and configured Port Forwarding to allow SSH through its protections. It's like a mini-firewall.

**VM -> Settings -> Network**

    Attached To: NAT

**Advanced-> Port Forwarding**

        Protocol: TCP
        Host IP: 127.0.0.1
        Host Port: 3022
        Guest IP: [VM_IP]
        Guest Port: 22

This means that the VM will forward all traffic from port 22 on our VM to port 3022 on our host machine.

Then, we'll be able to connect to port 3022 on our host (localhost) to access the VM's SSH instance.

### Pushing your public key (to the VM)

When we first connect, we do it with a password, remember, 'PasswordAuthentication yes' is still configured in our **sshd_config**

So we will connect with a password and then use **ssh-copy-id** to send over the public key we created earlier. Once our server has that, we will tell it to trust us *only* by our public key, and we will disable 'PasswordAuthentication.'

From your host machine, run the following
```bash
    ssh-copy-id -i ~/.ssh/VM_PUBLIC_KEY.pub kali@localhost -p 3022
```
Once you're successful, connect into SSH normally and edit sshd_config to disable PasswordAuthentication.
```bash
    ssh kali@localhost -p 3022
    ┌──(kali㉿kali)- sudo nano /etc/ssh/sshd_config

      PasswordAuthentication no
```
That's it, you're all set up.

### Running the VM headless

If you choose to, you can start your VM in headless mode (with no UI) either via the following terminal command
```bash
    VBoxManage startvm VM-name --type headless
```
Or by selecting **Start -> Headless Start** from the machine menu
